# kartel-audio-player

A simple audio player with ReactJS and CSS3.

Inspiration: https://dribbble.com/shots/998479-Music


To run it on your local environment:

```
yarn install
yarn start
yarn build // to build
```

Then open [`localhost:3000`](http://localhost:3000).

### Notes

Works perfectly in Chrome, Firefox and Safari. No tested in IE.

Version 2.0 was rewritten. Be careful.

## License

MIT License.

Copyright (c) 2017 Cezar Luiz.
